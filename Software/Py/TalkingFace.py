#!/usr/bin/env python
from PIL import Image, ImageDraw, ImageFont
import time
import secret
from neopixel import *
import paho.mqtt.client as mqtt
import argparse
import os
import random
import pyaudio
import wave
import numpy as np
import RPi.GPIO as GPIO

# RPi.GPIO Layout verwenden (wie Pin-Nummern)
GPIO.setmode(GPIO.BOARD)

# Pin 11 (GPIO 17) auf Input setzen
GPIO.setup(11, GPIO.IN)

# LED strip configuration:
LED_COUNT = 16  # Number of LED pixels.
#LED_PIN = 18  # GPIO pin connected to the pixels (18 uses PWM!).
LED_PIN            = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10  # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 8  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_ZigZac = False  # Leds gehen hin und zurück
LED_WIDTH = 8
LED_HEIGHT = 8
LED_PANEL = 5
LED_COUNT = LED_WIDTH * LED_HEIGHT * LED_PANEL  # Number of LED pixels.
MqttMsgData = ''
Aktion = 0
alterSound = 0
# INIT
zeit = 0.0
Talk = False
eyeLeft = 0
eyeRight = 1
mouth = 2
zwinkerLeftState = 0
zwinkerRightState = 0
zwinkerTriggerLeft = True
cycle = 0
simpleZwinker = [0, 1, 2, 3, 4, 3, 2, 1, 0]
xLeftEye = 3
yLeftEye = 3
xRightEye = 3
yRightEye = 3

FC_eyeLeft_r = 0
FC_eyeLeft_g = 0
FC_eyeLeft_b = 0

FC_eyeRight_r = 0
FC_eyeRight_g = 0
FC_eyeRight_b = 0

FC_eyeMouth_r = 0
FC_eyeMouth_g = 0
FC_eyeMouth_b = 0

BC_eyeLeft_r = 255
BC_eyeLeft_g = 255
BC_eyeLeft_b = 255

BC_eyeRight_r = 255
BC_eyeRight_g = 255
BC_eyeRight_b = 255

BC_eyeMouth_r = 255
BC_eyeMouth_g = 255
BC_eyeMouth_b = 255

lastTime = 0

mouthBMP = [Image.open('/home/pi/Talking_Face/Software/Py/PIC2/0000.bmp'),
         Image.open('/home/pi/Talking_Face/Software/Py/PIC2/0010.bmp'),
         Image.open('/home/pi/Talking_Face/Software/Py/PIC2/0020.bmp'),
         Image.open('/home/pi/Talking_Face/Software/Py/PIC2/0030.bmp'),
         Image.open('/home/pi/Talking_Face/Software/Py/PIC2/0040.bmp'),
         Image.open('/home/pi/Talking_Face/Software/Py/PIC2/0050.bmp'),
         Image.open('/home/pi/Talking_Face/Software/Py/PIC2/0060.bmp')]

aktBmpEyeLeft = Image.new('RGB', (LED_WIDTH, LED_HEIGHT), "black")
aktBmpEyeRight = Image.new('RGB', (LED_WIDTH, LED_HEIGHT), "black")
aktBmpMouth = Image.new('RGB', (LED_WIDTH * 3, LED_HEIGHT), "black")
aktBmp = Image.new('RGB', (LED_WIDTH * LED_PANEL, LED_HEIGHT), "black")


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.connected_flag=True #set flag




def callback(in_data, frame_count, time_info, status):
    global fdata
    data = wf.readframes(frame_count)
    fdata = data
    print("call")
    return (data, pyaudio.paContinue)


def init_eye(bmp):
    d = ImageDraw.Draw(bmp)
    d.point((0, 0), fill='black')
    d.point((0, 1), fill='black')
    d.point((1, 0), fill='black')
    d.point((7, 7), fill='black')
    d.point((7, 6), fill='black')
    d.point((6, 7), fill='black')
    d.point((7, 0), fill='black')
    d.point((6, 0), fill='black')
    d.point((7, 1), fill='black')
    d.point((0, 7), fill='black')
    d.point((1, 7), fill='black')
    d.point((0, 6), fill='black')

def chk_eye_move(direction,x , y):
    if direction == 1: # Up + Left
        x -= 1
        y -= 1
    if direction == 2: # Up + Left
        y -= 1
    if direction == 3: # Up + Left
        x += 1
        y -= 1
    if direction == 4: # Up + Left
        x += 1
    if direction == 5: # Up + Left
        x += 1
        y += 1
    if direction == 6: # Up + Left
        y += 1
    if direction == 7: # Up + Left
        x -= 1
        y += 1
    if direction == 8: # Up + Left
        x -= 1

    if  x < 1 or x > 5 or y < 1 or y > 5:
        return -1, -1
    if (x == 0 and y == 0) or (x == 1 and y == 0) or (x == 0 and y == 1) or (x == 1 and y == 1):
        return -1, -1
    if (x == 0 and y == 6) or (x == 1 and y == 6) or (x == 0 and y == 5) or (x == 1 and y == 5):
        return -1, -1
    if (x == 5 and y == 0) or (x == 6 and y == 1) or (x == 6 and y == 0 or (x == 5 and y == 1)):
        return -1, -1
    if (x == 5 and y == 7) or (x == 6 and y == 6) or (x == 6 and y == 7) or (x == 5 and y == 5):
        return -1, -1
    return x, y


def zwinker_eye(bmp, state):

    d = ImageDraw.Draw(bmp)
    for i in range(state):
        d.line([(0, 0+i), (8, 0+i)], fill='black')
        d.line([(0, 7-i), (8, 7-i)], fill='black')


def pupille(bmp, xy, Farbe):
    x, y = xy
    d = ImageDraw.Draw(bmp)
    d.rectangle([(x, y), (x+1, y+1)], fill=Farbe)


def fill_stripe_ram(panelAnzahl, width, height, BMP):
    # print('fillStripeRam')

    for PanelNr in range(panelAnzahl):
        for row in range(height):
            for column in range(width):

                if LED_ZigZac and ((row + 1) & 1):
                    pixelNr = (PanelNr * width * height) + column + (row * width)
                else:
                    pixelNr = (PanelNr * width * height) + (row * width) + (width - column) - 1
                if not LED_ZigZac:
                    pixelNr = (PanelNr * width * height) + column + (row * width)

                # print(PanelNr, column, row)
                # print('Farbe: %x', pixelNr)
                r, g, b = BMP.getpixel(((column + PanelNr * width), row))
                # print(r, g, b)
                strip.setPixelColor(pixelNr, Color(g, r, b))

def MundAnimation(in_data, zähler):
    data1 = np.fromstring(in_data, dtype=np.int16)
    dataL = data1[0::2]
    dataR = data1[1::2]
    try:
        Nr= 0
        peakL = np.abs(np.max(dataL) - np.min(dataL)) / maxValue
        peakR = np.abs(np.max(dataR) - np.min(dataR)) / maxValue
        print(peakL)
        if peakL > 0.1:
            Nr = random.randint(1, 6)
        else:
            zähler += 1
            if zähler > 10:
                Nr = 0
                zähler = 0
    except ValueError:
        print ("Error: can\'t find file or read data")

    return Nr, zähler

def Bewegung():
    if GPIO.input(11) == GPIO.HIGH:
        # LED an
        print("AN")
        return True
    else:
        print("Aus")
        return False


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    lastTime = time.clock()
    print('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:




        while True:
            zeit -= 1
            LED_BRIGHTNESS = 10
            if int(time.strftime("%H")) < 19:
                LED_BRIGHTNESS = 20

            if int(time.strftime("%H")) < 18:
                LED_BRIGHTNESS = 50


            strip.setBrightness(LED_BRIGHTNESS)
            if Bewegung() and zeit<0:
                    zeit= 30.0
                    while 1:
                        sounddat = random.randint(1, 22)
                        if int(time.strftime("%H"))<20:
                            sounddat=random.randint(1, 23)
                        if int(time.strftime("%H"))<19:
                            sounddat=random.randint(1, 24)
                        if int(time.strftime("%H"))<18:
                            sounddat=random.randint(1, 25)
                        if sounddat != alterSound:
                            alterSound = sounddat
                            break
                    maxValue = 2 ** 16

                    chunk = 1024
                    wf = wave.open('/home/pi/Talking_Face/Software/Py/sound/'+str(sounddat)+'.wav', 'rb')




                    p = pyaudio.PyAudio()
                    stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                                    channels=wf.getnchannels(),
                                    rate=wf.getframerate(),
                                    output=True,
                                    stream_callback=callback)
                    data = 0
                    # start the stream (4)
                    fdata = wf.readframes(512)
                    stream.start_stream()

                    # wait for stream to finish (5)
                    zähler = 0

                    while stream.is_active():
                        BildNr, zähler = MundAnimation(fdata,zähler)
                        aktBmpMouth = mouthBMP[BildNr]
                        randomEye1 = random.randint(1, 50)
                        randomZwinker1 = random.randint(0, 100)
                        randomEye2 = random.randint(1, 50)
                        randomZwinker2 = random.randint(0, 100)
                        randomCrazyEys = random.randint(1, 1)
                        randomCrazyEys = False
                        if not zwinkerTriggerLeft:
                            x, y = chk_eye_move(randomEye1, xLeftEye, yLeftEye)
                            if x != -1 and y != -1:
                                xLeftEye = x
                                yLeftEye = y
                                xRightEye = x
                                yRightEye = y
                            if randomCrazyEys:
                                x, y = chk_eye_move(randomEye2, xRightEye, yRightEye)
                            if x != -1 and y != -1:
                                xRightEye = x
                                yRightEye = y
                        if randomZwinker1 == 1:
                            zwinkerTriggerLeft = True
                        # ---------------------------------------------------------------
                        # set BMP to Background Color
                        aktBMP = Image.new('RGB', (LED_WIDTH * LED_PANEL, LED_HEIGHT), 'black')
                        aktBmpEyeLeft = Image.new('RGB', (8, 8), (BC_eyeLeft_r, BC_eyeLeft_g, BC_eyeLeft_b))
                        aktBmpEyeRight = Image.new('RGB', (8, 8), (BC_eyeRight_r, BC_eyeRight_g, BC_eyeRight_b))
                        # ---------------------------------------------------------------
                        # put Text to the Bitmap
                        # text()
                        init_eye(aktBmpEyeLeft)
                        init_eye(aktBmpEyeRight)
                        pupille(aktBmpEyeLeft, (xLeftEye, yLeftEye), (FC_eyeLeft_r, FC_eyeLeft_g, FC_eyeLeft_b))
                        pupille(aktBmpEyeRight, (xRightEye, yRightEye), (FC_eyeRight_r, FC_eyeRight_g, FC_eyeRight_b))

                        if zwinkerTriggerLeft:

                            zwinker_eye(aktBmpEyeLeft, simpleZwinker[zwinkerLeftState])
                            zwinker_eye(aktBmpEyeRight, simpleZwinker[zwinkerLeftState])
                            zwinkerLeftState += 1
                            if zwinkerLeftState == len(simpleZwinker):
                                zwinkerLeftState = 0
                                zwinkerTriggerLeft = False

                        # left_eye_draw()
                        aktBMP.paste(aktBmpEyeLeft, box=(eyeLeft * 8, 0))
                        aktBMP.paste(aktBmpEyeRight, box=(eyeRight * 8, 0))
                        aktBMP.paste(aktBmpMouth, box=(mouth*8, 0))
                        fill_stripe_ram(LED_PANEL, LED_WIDTH, LED_HEIGHT, aktBMP)
                        # copy Bitmap to RGB Matrix ram

                        # ---------------------------------------------------------------
                        # wait for minimum 80ms

                        # ---------------------------------------------------------------
                        # Write RGB Matrix Hardware
                        while (time.clock() - lastTime) < 0.1:  # wait until 50ms are done
                            a = 1


                            if (time.clock() - lastTime) < 0:
                                # needs mor thinking
                                lastTime = time.clock()
                                # needs mor thinking
                        lastTime = time.clock()
                        print ("stripe")
                        strip.show()

                    stream.stop_stream()
                    stream.close()
                    wf.close()

                    p.terminate()
                    print ('fertig')
            else:
                               
                aktBmpMouth = mouthBMP[0]
                randomEye1 = random.randint(1, 50)
                randomZwinker1 = random.randint(0, 100)
                randomEye2 = random.randint(1, 50)
                randomZwinker2 = random.randint(0, 100)
                randomCrazyEys = random.randint(1, 1)
                randomCrazyEys = False
                if not zwinkerTriggerLeft:
                    x, y = chk_eye_move(randomEye1, xLeftEye, yLeftEye)
                    if x != -1 and y != -1:
                        xLeftEye = x
                        yLeftEye = y
                        xRightEye = x
                        yRightEye = y
                    if randomCrazyEys:
                        x, y = chk_eye_move(randomEye2, xRightEye, yRightEye)
                    if x != -1 and y != -1:
                        xRightEye = x
                        yRightEye = y
                if randomZwinker1 == 1:
                    zwinkerTriggerLeft = True
                # ---------------------------------------------------------------
                # set BMP to Background Color
                aktBMP = Image.new('RGB', (LED_WIDTH * LED_PANEL, LED_HEIGHT), 'black')
                aktBmpEyeLeft = Image.new('RGB', (8, 8), (BC_eyeLeft_r, BC_eyeLeft_g, BC_eyeLeft_b))
                aktBmpEyeRight = Image.new('RGB', (8, 8), (BC_eyeRight_r, BC_eyeRight_g, BC_eyeRight_b))
                # ---------------------------------------------------------------
                # put Text to the Bitmap
                # text()
                init_eye(aktBmpEyeLeft)
                init_eye(aktBmpEyeRight)
                pupille(aktBmpEyeLeft, (xLeftEye, yLeftEye), (FC_eyeLeft_r, FC_eyeLeft_g, FC_eyeLeft_b))
                pupille(aktBmpEyeRight, (xRightEye, yRightEye), (FC_eyeRight_r, FC_eyeRight_g, FC_eyeRight_b))

                if zwinkerTriggerLeft:

                    zwinker_eye(aktBmpEyeLeft, simpleZwinker[zwinkerLeftState])
                    zwinker_eye(aktBmpEyeRight, simpleZwinker[zwinkerLeftState])
                    zwinkerLeftState += 1
                    if zwinkerLeftState == len(simpleZwinker):
                        zwinkerLeftState = 0
                        zwinkerTriggerLeft = False

                # left_eye_draw()
                aktBMP.paste(aktBmpEyeLeft, box=(eyeLeft * 8, 0))
                aktBMP.paste(aktBmpEyeRight, box=(eyeRight * 8, 0))
                aktBMP.paste(aktBmpMouth, box=(mouth*8, 0))
                fill_stripe_ram(LED_PANEL, LED_WIDTH, LED_HEIGHT, aktBMP)
                # copy Bitmap to RGB Matrix ram

                # ---------------------------------------------------------------
                # wait for minimum 80ms

                # ---------------------------------------------------------------
                # Write RGB Matrix Hardware
                while (time.clock() - lastTime) < 0.1:  # wait until 50ms are done
                    a = 1


                    if (time.clock() - lastTime) < 0:
                        # needs mor thinking
                        lastTime = time.clock()
                        # needs mor thinking
                lastTime = time.clock()
                print ("stripe")
                strip.show()

            
            # ---------------------------------------------------------------


            # print(lastTime)

    except KeyboardInterrupt:

        if args.clear:
            print('ende')
